extends PanelContainer
var Nombre=preload("res://UI/Nombre.tscn")

func _ready():
	pass

func _on_Button_pressed():
	var Nuevo=Nombre.instance()
	Nuevo.connect("Nombre",get_parent(),"setName")
	Nuevo.connect("Nombre",self,"cerrar")
	add_child(Nuevo)

func cerrar(dato):
	queue_free()
