extends Control
var Name
var Score= 0

var PlanetName=preload("res://UI/NombrePlaneta.tscn")
var Planet=preload("res://Planeta.tscn")
var CityName=preload("res://UI/NombreCiudad.tscn")

func _ready():
	pass # Replace with function body.

func setName(Nombre):
	Name=Nombre
	$Panel/TopBar/GridContainer/Label.text=Name
	$Panel/TopBar.visible=true
	$Panel/LeftBar.visible=true


func _on_Button_pressed():
	var Nuevo=PlanetName.instance()
	Nuevo.connect("Nombre",self,"crearPlaneta")
	add_child(Nuevo)
	
func crearPlaneta(Nombre):
	var Nuevo=Planet.instance()
	Nuevo.Nombre=Nombre
	add_child(Nuevo)
	var New=CityName.instance()
	New.connect("Nombre",Nuevo,"capital_Name")
	add_child(New)
	
